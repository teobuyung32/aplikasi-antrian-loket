<?php
require 'functions.php';
$us = query("SELECT * FROM Pasien")
?>

<html>
<link rel="stylesheet" type="text/css" href="../aplikasiantrianloket/css/S_Adminn.css" />
    <head>
        <title> Halaman Admin</title>
</head>
<div>
<body>
	<!-- NAVIGATION BAR -->
	<div class="navbar-container">
		<ul class="ul-navbar">
		<li class="li-navbar">
			DAFTAR ANTRIAN</li>	
		</ul>
	</div>
	<!-- NAVIGATION BAR SELESAI -->

	<!-- CONTENT 1 -->
	<div class="cont">
    <table border="1" cellpadding="10" cellspacing="0" class="table">
        <tr>
            
            <th>No</th>
            <th>Pasien Selesai</th>
            <th>Nama</th>
            <th>Nik</th>
            <th>Alamat</th>
            <th>No Hp</th>
            <th>Keluhan</th>
            <th>BPJS</th>
        </tr>
        <?php $i = 1; ?>
        <?php foreach( $us as $row ): ?>
        <tr>
            <td><?= $i; ?></td>
            <td>
                <a class="tombol" href="hapus.php?id=<?= $row["id"]; ?>">Selesai</a></td>
            <td><?= $row["Nama"]; ?> </td>
            <td><?= $row["NIK"]; ?> </td>
            <td><?= $row["Alamat"]; ?> </td>
            <td><?= $row["NoHp"]; ?> </td>
            <td><?= $row["Keluhan"]; ?> </td>
            <td><?= $row["BPJS"]; ?> </td>
        </tr>
        <?php $i++; ?>
        <?php endforeach; ?>
    </table>
    <div class="tmb">
    <a class ="tombol-1" href="logout.php">
                            <img src="icon/logout.png"  />
                            <span>Keluar</span></a>
    <a class ="tombol-2" href="pengguna.php">
                            <img src="icon/admin.png"  />
                            <span>Tambah Admin</span></a>
    </div>
	</div>
	<!-- CONTENT 1 END -->
	
	<!-- FOOTER -->
	<div class="footer">
		<center><h1 class="footer-h1"> Antrian Loket by: Teo Buyung W. </h1></center> 
	</div>
	<!-- FOOTER END -->
	</div>
</body>
</html>