<?php
require 'functions.php';
$us = query("SELECT * FROM Pasien")
?>

<html>
<link rel="stylesheet" type="text/css" href="../aplikasiantrianloket/css/S_Antrian.css" />
    <head>
        <title> Halaman Admin</title>
</head>
<body>
<nav>
        <div class="logo">
            <img src="icon/das.png" class="menu-icon" />
            <span class="logo-name">JUMLAH ANTRIAN</span>
        </div>
        <div class="container">
            <div class="logo">
                <img src="icon/das.png" class="menu-icon" />
                <span class="logo-name">Menu</span>
            </div>

            <div class="container-menu">
                <ul class="ul-navbar">
                    <li class="li-navbar">
                        <a href="dasboard.html" class="navigasi">
                            <img src="icon/das.png" class="icon" />
                            <span class="link">DASHBOARD</span>
                        </a>
                    </li>
                    <li class="li-navbar">
                        <a href="Antrian.php" class="navigasi">
                            <img src="icon/antri.png" class="icon" />
                            <span class="link">JUMLAH ANTRIAN</span>
                        </a>
                    </li>
                    <li class="li-navbar">
                        <a href="login.php" class="navigasi">
                             <img src="icon/user.png" class="icon" />
                              <span class="link">ADMIN</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
	<div class="cont">
    <table border="1" cellpadding="10" cellspacing="0" class="table">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Alamat</th>
        </tr>
        <?php $i = 1; ?>
        <?php foreach( $us as $row ): ?>
        <tr>
            <td><?= $i; ?></td>
            <td><?= $row["Nama"]; ?> </td>
            <td><?= $row["Alamat"]; ?> </td>
        </tr>
        <?php $i++; ?>
        <?php endforeach; ?>
    </table>
		
	</div>
	<div class="footer">
		<center><h1 class="footer-h1"> TERMIKASIH TELAH MENDAFTAR SILAHKAN TUNGGU DI RUANG YANG TELAH DISEDIAKAN </h1></center> 
	</div>

    <section class="overlay"></section>
    <script src="main.js"></script>

	
</body>
</html>